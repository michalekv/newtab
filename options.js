function save_options() {
    let color = document.getElementById('color').value;
    chrome.storage.sync.set({
        backgroundColor: color,
    }, function() {
        var status = document.getElementById('status');
        status.textContent = 'Options saved.';
        setTimeout(function() {
            status.textContent = '';
        }, 750);
    });
}

function restore_options() {
    chrome.storage.sync.get({
        backgroundColor: 'white'
    }, function(items) {
        document.getElementById('color').value = items.backgroundColor;
    });
}
document.addEventListener('DOMContentLoaded', restore_options);
document.getElementById('save').addEventListener('click', save_options);