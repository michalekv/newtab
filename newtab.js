'use strict';

chrome.storage.sync.get('backgroundColor', function(data) {
    document.body.style.backgroundColor = data.backgroundColor;
});